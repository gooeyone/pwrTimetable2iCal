# PWr Timetable iCal Converter

This short python script lets you convert your timetable from *edukacja* to *.ical* format, so it can be imported into your *Google Calendar*.

## Prerequisites

In order to be able to use this script you should have **python3.5** installed and some additional libraries listed here. Below you can find commands you can use to install these libraries with *pip*.

```bash
pip install beautifulsoup4
pip install icalendar
```

*If there are any libraries you had to install to be able to use this script, please let me know.*

## How to convert my timetable to .ical format?

*Remember that this script should be called with **python3.5**. It was not tested with other versions.*

First of all you have to obtain html source with your timetable. In order to do that go to [edukacja](edukacja.pwr.wroc.pl) and sign in. Make sure you are on *Studia* tab. Then go to *Zapisy* and choose *Przełącz na <Grupy zajęciowe, do których słuchacz jest zapisany w semestrze>*. Then right click on this page and choose *Page Source*. You have to save page's source into a file with .html extension.

All you have to do next is just call the script with a single parameter containing path to HTML file containing source from *edukacja*.

```
python3.5 convert2ical.py mySchedule.html
```

After above command is invoked you should notice *calendar.ical* file in current directory. Now you can import your timetable into your favourite external calendar tool. :)
