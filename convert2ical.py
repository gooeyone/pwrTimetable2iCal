from bs4 import BeautifulSoup
from icalendar import Calendar, Event

import re
import datetime
import argparse

# process input parameters (html filename)
parser = argparse.ArgumentParser()
parser.add_argument("timetable_source_file")
args = parser.parse_args()

# read timetable html source
with open(args.timetable_source_file, 'r', encoding='utf8') as input_schedule:
    soup = BeautifulSoup(input_schedule, 'html.parser')


# get table with timetable from html source
# TODO: figure out something more clever to find proper table
tables = soup.find_all('table', class_='KOLOROWA')
timetable = tables[2]

# get rows with courses data
# TODO: find some better way
rows = timetable.find_all('tr', recursive=False)
del rows[0:4]

# extract classes details
courses = { 'course_code':[],
            'course_name':[],
            'instructor':[],
            'course_type':[],
            'group_code':[],
            'schedule':[],
            'ects':[] }

row_num = 0
for row in rows:
    if row_num % 4 == 0:
        courses['group_code'].append( row.find_all('td', recursive=False)[0].string )
        courses['course_code'].append( row.find_all('td', recursive=False)[1].string )
        courses['course_name'].append( row.find_all('td', recursive=False)[2].string )
    elif row_num % 4 == 2:
        courses['instructor'].append( row.find_all('td', recursive=False)[0].string )
        courses['course_type'].append( row.find_all('td', recursive=False)[1].string )
        courses['ects'].append( row.find_all('td', recursive=False)[7].string )
    elif row_num % 4 == 3:
        schedule_rows = row.find_all('td', recursive=False)[0].table.find_all('td')
        schedule_table = []

        for schedule_row in schedule_rows:
            date = re.search('[0-9]{4}-[0-9]{2}-[0-9]{2}', schedule_row.text).group()
            time = re.search('[0-9]{2}:[0-9]{2}-[0-9]{2}:[0-9]{2}', schedule_row.text).group()
            location = re.search(', (.*,.*$)', schedule_row.text).group(1)
            schedule_table.append( (date, time, location) )

        courses['schedule'].append(schedule_table)

    row_num += 1

# clear data
for k in courses:
    if k == 'schedule':
        continue
    for i in range(0, len(courses[k])):
        courses[k][i] = re.sub(r'\n', '', courses[k][i])
        courses[k][i] = re.sub(r'^[ ]*', '', courses[k][i])
        courses[k][i] = re.sub(r'[ ]*$', '', courses[k][i])

# create calendar
cal = Calendar()
for i in range(0, len(courses['course_name'])):

    for course_class in courses['schedule'][i]:
        event = Event()
        summary =   courses['course_name'][i] + " (" +  \
                    courses['course_type'][i] + ")"
        description =   courses['instructor'][i] + '\n' + \
                        "kod kursu: " + courses['course_code'][i] + '\n' + \
                        "kod grupy: " + courses['group_code'][i] + '\n' +   \
                        'ECTS: ' + re.search('([0-9]{1,2}),', courses['ects'][i]).group(1)
        location = courses['schedule'][i][0][2]
        date_re = re.search('([0-9]{4})-([0-9]{2})-([0-9]{2})', course_class[0])
        time_re = re.search('([0-9]{2}):([0-9]{2})-([0-9]{2}):([0-9]{2})', course_class[1])
        starttime = datetime.datetime(  int(float(date_re.group(1))),    \
                                        int(float(date_re.group(2))),    \
                                        int(float(date_re.group(3))),    \
                                        int(float(time_re.group(1))),    \
                                        int(float(time_re.group(2))))
        endtime = datetime.datetime(  int(float(date_re.group(1))),      \
                                        int(float(date_re.group(2))),    \
                                        int(float(date_re.group(3))),    \
                                        int(float(time_re.group(3))),    \
                                        int(float(time_re.group(4))))
        event.add('dtstart', starttime)
        event.add('dtend', endtime)
        event.add('summary', summary)
        event.add('description', description)
        event.add('location', location)
        cal.add_component(event)

# save into .ical file
with open('calendar.ical', 'wb') as ical_file:
    ical_file.write( cal.to_ical() )

print("Timetable from %s file has been converted to .ical format (calendar.ical)." % args.timetable_source_file)
